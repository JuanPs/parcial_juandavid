/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.parcialCorte1.services;

import com.parcialCorte1.entidades.Enfermedades;
import com.parcialCorte1.exepciones.EnfermedadesException;
import com.parcialCorte1.modelo.dto.RespuestaDTO;
import com.parcialCorte1.validadores.ValidadorEnfermedades;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author juand
 */
@Stateless
@Path("com.parcialcorte1.entidades.enfermedades")
public class EnfermedadesFacadeREST extends AbstractFacade<Enfermedades> {

    @PersistenceContext(unitName = "parcialCorte1PU")
    private EntityManager em;

    public EnfermedadesFacadeREST() {
        super(Enfermedades.class);
    }

     @POST
    @Consumes({MediaType.APPLICATION_JSON})
    public RespuestaDTO createInfante(Enfermedades entity) {

        RespuestaDTO respuesta = new RespuestaDTO();
        try {
            ValidadorEnfermedades.validarDatosObligatorios(entity);
            //Validar si existe
            if (super.find(entity.getCodigo()) == null) {
               // ValidadorEnfermedades.validarDatosObligatorios(entity.getCodigo());
                super.create(entity);
                respuesta.setCodigo("200");
                respuesta.setExito(true);
                respuesta.setMensaje("Guardado con éxito");
                respuesta.setData(entity);
            } else {
                respuesta.setCodigo("301");
                respuesta.setExito(false);
                respuesta.setMensaje("Ya existe una enfermedad con"
                        + "el codigo " + entity.getCodigo());
            }
        } catch (EnfermedadesException ex) {
            respuesta.setCodigo("301");
            respuesta.setExito(false);
            respuesta.setMensaje(ex.getMessage());
        }

        return respuesta;
    }
    
    @POST
    @Override
    @Consumes({ MediaType.APPLICATION_JSON})
    public void create(Enfermedades entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({ MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") Integer id, Enfermedades entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({ MediaType.APPLICATION_JSON})
    public Enfermedades find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({ MediaType.APPLICATION_JSON})
    public List<Enfermedades> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({ MediaType.APPLICATION_JSON})
    public List<Enfermedades> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
