/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.parcialCorte1.services;

import com.parcialCorte1.entidades.Enfermedades;
import com.parcialCorte1.entidades.Pinturas;
import com.parcialCorte1.exepciones.EnfermedadesException;
import com.parcialCorte1.exepciones.PinturasException;
import com.parcialCorte1.modelo.dto.RespuestaDTO;
import com.parcialCorte1.validadores.ValidadorEnfermedades;
import com.parcialCorte1.validadores.ValidadorPinturas;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author juand
 */
@Stateless
@Path("com.parcialcorte1.entidades.pinturas")
public class PinturasFacadeREST extends AbstractFacade<Pinturas> {

    @PersistenceContext(unitName = "parcialCorte1PU")
    private EntityManager em;

    public PinturasFacadeREST() {
        super(Pinturas.class);
    }

       @POST
    @Consumes({MediaType.APPLICATION_JSON})
    public RespuestaDTO createInfante(Pinturas entity) {

        RespuestaDTO respuesta = new RespuestaDTO();
        try {
            ValidadorPinturas.validarDatosObligatorios(entity);
            //Validar si existe
            if (super.find(entity.getCodigo()) == null) {
               
                super.create(entity);
                respuesta.setCodigo("200");
                respuesta.setExito(true);
                respuesta.setMensaje("Guardado con éxito");
                respuesta.setData(entity);
            } else {
                respuesta.setCodigo("301");
                respuesta.setExito(false);
                respuesta.setMensaje("Ya existe una Pintura con"
                        + "el codigo " + entity.getCodigo());
            }
        } catch (PinturasException ex) {
            respuesta.setCodigo("301");
            respuesta.setExito(false);
            respuesta.setMensaje(ex.getMessage());
        }

        return respuesta;
    }
    
    @POST
    @Override
    @Consumes({ MediaType.APPLICATION_JSON})
    public void create(Pinturas entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({ MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") Integer id, Pinturas entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({ MediaType.APPLICATION_JSON})
    public Pinturas find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({ MediaType.APPLICATION_JSON})
    public List<Pinturas> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({ MediaType.APPLICATION_JSON})
    public List<Pinturas> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
