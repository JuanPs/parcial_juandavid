/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.parcialCorte1.validadores;


import com.parcialCorte1.entidades.Enfermedades;
import com.parcialCorte1.exepciones.EnfermedadesException;
import javax.ejb.EJB;

/**
 *
 * @author David
 */
public class ValidadorEnfermedades {
   
    public static void validarDatosObligatorios(Enfermedades enfermedades) 
            throws EnfermedadesException
    {
        if(enfermedades.getCurable()==null || 
                enfermedades.getCurable().equals(""))
        {
            throw new EnfermedadesException("Debe diligenciar la identificación");
        }
        if(enfermedades.getNombre()==null || enfermedades.getNombre().equals(""))
        {
            throw new EnfermedadesException("Debe diligenciar el nombre");        
        }
       
        
    }
    
   
    
}
