/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.parcialCorte1.validadores;

import com.parcialCorte1.entidades.Pinturas;
import com.parcialCorte1.exepciones.PinturasException;

/**
 *
 * @author juand
 */
public class ValidadorPinturas {
    
        public static void validarDatosObligatorios(Pinturas pinturas ) 
            throws PinturasException
    {
        if(pinturas.getDescripcion()==null || 
                pinturas.getDescripcion().equals(""))
        {
            throw new PinturasException("Debe diligenciar la Descripcion");
        }
        if(pinturas.getColor()==null || pinturas.getColor().equals(""))
        {
            throw new PinturasException("Debe diligenciar el color");        
        }
        if(pinturas.getPrecio()==null || pinturas.getPrecio().equals(""))
        {
            throw new PinturasException("Debe diligenciar el Precio");        
        }
        
    }
}
