/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.parcialCorte1.controlador;

import com.parcialCorte1.entidades.Pinturas;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author juand
 */
@Stateless
public class PinturasFacade extends AbstractFacade<Pinturas> {

    @PersistenceContext(unitName = "parcialCorte1PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PinturasFacade() {
        super(Pinturas.class);
    }
    
}
