/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.parcialCorte1.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author juand
 */
@Entity
@Table(name = "pinturas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pinturas.findAll", query = "SELECT p FROM Pinturas p")
    , @NamedQuery(name = "Pinturas.findByColor", query = "SELECT p FROM Pinturas p WHERE p.color = :color")
    , @NamedQuery(name = "Pinturas.findByDescripcion", query = "SELECT p FROM Pinturas p WHERE p.descripcion = :descripcion")
    , @NamedQuery(name = "Pinturas.findByPrecio", query = "SELECT p FROM Pinturas p WHERE p.precio = :precio")
    , @NamedQuery(name = "Pinturas.findByCodigo", query = "SELECT p FROM Pinturas p WHERE p.codigo = :codigo")})
public class Pinturas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Size(max = 20)
    @Column(name = "color")
    private String color;
    @Size(max = 40)
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "precio")
    private Integer precio;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "codigo")
    private Integer codigo;

    public Pinturas() {
    }

    public Pinturas(Integer codigo) {
        this.codigo = codigo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getPrecio() {
        return precio;
    }

    public void setPrecio(Integer precio) {
        this.precio = precio;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigo != null ? codigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pinturas)) {
            return false;
        }
        Pinturas other = (Pinturas) object;
        if ((this.codigo == null && other.codigo != null) || (this.codigo != null && !this.codigo.equals(other.codigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.parcialCorte1.entidades.Pinturas[ codigo=" + codigo + " ]";
    }
    
}
