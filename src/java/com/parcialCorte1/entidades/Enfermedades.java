/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.parcialCorte1.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author juand
 */
@Entity
@Table(name = "enfermedades")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Enfermedades.findAll", query = "SELECT e FROM Enfermedades e")
    , @NamedQuery(name = "Enfermedades.findByNombre", query = "SELECT e FROM Enfermedades e WHERE e.nombre = :nombre")
    , @NamedQuery(name = "Enfermedades.findByCurable", query = "SELECT e FROM Enfermedades e WHERE e.curable = :curable")
    , @NamedQuery(name = "Enfermedades.findByDescripcion", query = "SELECT e FROM Enfermedades e WHERE e.descripcion = :descripcion")
    , @NamedQuery(name = "Enfermedades.findByCodigo", query = "SELECT e FROM Enfermedades e WHERE e.codigo = :codigo")})
public class Enfermedades implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 25)
    @Column(name = "curable")
    private String curable;
    @Size(max = 50)
    @Column(name = "descripcion")
    private String descripcion;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "codigo")
    private Integer codigo;

    public Enfermedades() {
    }

    public Enfermedades(Integer codigo) {
        this.codigo = codigo;
    }

    public Enfermedades(Integer codigo, String nombre) {
        this.codigo = codigo;
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCurable() {
        return curable;
    }

    public void setCurable(String curable) {
        this.curable = curable;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigo != null ? codigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Enfermedades)) {
            return false;
        }
        Enfermedades other = (Enfermedades) object;
        if ((this.codigo == null && other.codigo != null) || (this.codigo != null && !this.codigo.equals(other.codigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.parcialCorte1.entidades.Enfermedades[ codigo=" + codigo + " ]";
    }
    
}
