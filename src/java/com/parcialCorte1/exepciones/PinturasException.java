/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.parcialCorte1.exepciones;

/**
 *
 * @author Daniela
 */
public class PinturasException extends Exception{

    public PinturasException(String string) {
        super(string);
    }

    public PinturasException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

    public PinturasException(Throwable thrwbl) {
        super(thrwbl);
    }
    
}
