/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.parcialCorte1.exepciones;

/**
 *
 * @author juand
 */
public class EnfermedadesException  extends Exception {
    
      public EnfermedadesException(String string) {
        super(string);
    }

    public EnfermedadesException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

    public EnfermedadesException(Throwable thrwbl) {
        super(thrwbl);
    }
    
}

/**
 *
 * @author Daniela
 */

